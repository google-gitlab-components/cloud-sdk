# run-gcloud

**Status: [Beta][beta]**

> - `google_cloud_support_feature_flag` (Beta) flag needs to be enabled to use the Component.

The `run-gcloud` GitLab component executes [Google Cloud CLI][google-cloud-cli] commands.

The component uses a customized gcloud image instead of the [google/cloud-sdk][google-cloud-sdk] image to reduce the image size, avoid security vulnerabilities.

## Prerequisites

- Create a Google Cloud project.
- Set up a Google Cloud workload identity pool and provider by following the steps in the GitLab tutorial [Google Cloud workload identity federation and IAM policies][onboarding].

## Billing

Use of the `run-gcloud` GitLab component might incur Google Cloud billing charges depending on your usage and the resources you create. For more information
on the the `gcloud` CLI pricing, see [Google Cloud SDK Pricing][pricing] and the pricing pages of the Google Cloud products you intend to use.

## Usage

Add the following to your `.gitlab-ci.yml` file:

```yaml
include:
  - component:  gitlab.com/google-gitlab-components/run-gcloud@<VERSION>
    inputs:
      project_id: <GOOGLE-PROJECTID>
      commands: |
          gcloud version
          gcloud info
```

- `<VERSION>` is the release tag (e.g. `0.1.0`) or main of the `run-gcloud` component.
- `<GOOGLE-PROJECTID>` is the [Google Project ID][google-project], which will be used to configure the Google Cloud project.

You can use the `run-gcloud` component to access Google Cloud services that currently not supported by existing [Google Cloud GitLab Components][google-gitlab-components]. For example, to list the GCEs in the project:

```yaml
include:
  - component:  gitlab.com/google-gitlab-components/run-gcloud@<VERSION>
    inputs:
      project_id: <GOOGLE-PROJECTID>
      commands: |
          gcloud compute instances list
```

You can also use `run-gcloud` component to run a preparation job before other jobs.
The following example creates the [Cloud Deploy Pipeline][cloud-deploy-pipeline] using `run-gcloud` component before using the
`create-cloud-deploy-release` component.

```yaml
include:
  - component: gitlab.com/google-gitlab-components/run-gcloud
    inputs:
      stage: .pre
      commands: "gcloud deploy apply --file=clouddeploy.yaml --region=us-central1"

include:
  - component: gitlab.com/google-gitlab-components/cloud-deploy/create-cloud-deploy-release@0.1.0
    inputs:
      project_id: my-project
      name: release-$CI_PIPELINE_ID
      delivery_pipeline: my-delivery-pipeline
      region: us-central1
      images: img1=path/to/img1,img2=path/to/img2
```

## Inputs

The following table lists inputs for the `run-gcloud` component

| Input | Description | Example| Default value |
| ----- | ------- | ------------- | ----------- |
| `project_id` | (Required) project_id is the Google Cloud Project ID | `my-project` | 	|
| `commands` | (Required) commands are the commands need to be executed | `gcloud info` |   |
| `as` | (Optional) The name of the job to be executed| `my-job` | `run-gcloud` |
| `stage` | (Optional) The GitLab CI/CD stage | `my-stage`  | `build`  |

## Authorization

Grant the workload identity pool the required roles for the Google Cloud services that you want to use with the `run-gcloud` component

For example, if you want to use the `run-gcloud` component to read from Google Cloud Artifact Registry, grant the workload identity pool the [Artifact Registry Reader][sa-ar-reader]  role.

To grant the `roles/artifactregistry.reader` role to all principals in a workload identity pool matching `developer_access=true` attribute mapping using the gcloud CLI, run the following command:

``` bash
# Replace ${PROJECT_ID}, ${PROJECT_NUMBER}, ${LOCATION}, ${POOL_ID} with your values below

WORKLOAD_IDENTITY=principalSet://iam.googleapis.com/projects/${PROJECT_NUMBER}/locations/global/workloadIdentityPools/${POOL_ID}/attribute.developer_access/true

gcloud projects add-iam-policy-binding ${PROJECT_ID} --member="${WORKLOAD_IDENTITY}" --role="roles/artifactregistry.reader"
```


[beta]: https://docs.gitlab.com/ee/policy/experiment-beta-support.html#beta
[google-cloud-sdk]: https://hub.docker.com/r/google/cloud-sdk/
[google-cloud-cli]: https://cloud.google.com/cli
[onboarding]: https://docs.gitlab.com/ee/integration/google_cloud_iam.html
[google-project]:  https://cloud.google.com/resource-manager/docs/creating-managing-projects#identifying_projects
[sa-ar-reader]: https://cloud.google.com/iam/docs/understanding-roles#artifactregistry.reader
[cloud-deploy-pipeline]: https://cloud.google.com/deploy/docs/create-pipeline-targets
[google-gitlab-components]: https://gitlab.com/google-gitlab-components
[pricing]: https://cloud.google.com/sdk#pricing